import os
from PipelineWriter import PipelineWriter


def get_libs():
    libs_path = "./"
    libs = os.listdir(libs_path)
    return libs

def generator():
    with open('child-pipeline-gitlab-ci.yml', 'w+') as f:
        f.write(PipelineWriter.parent_job_template())
        for lib in get_libs():
            f.write(PipelineWriter.child_pipeline_job_template(lib))

def generator():
    with open('child-pipeline-gitlab-ci.yml', 'w+') as f:
        f.write(PipelineWriter.header())
        f.write(PipelineWriter.dynamic_pipeline("multi-branch-2"))
        f.write(PipelineWriter.dynamic_pipeline("multi-branch-1"))
        f.write(PipelineWriter.dynamic_pipeline("multi-branch-0"))
        f.write(PipelineWriter.dynamic_pipeline("multi-branch-3"))


if __name__ == "__main__":
    generator()
