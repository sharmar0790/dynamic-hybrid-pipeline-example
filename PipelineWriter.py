class PipelineWriter:

#     @staticmethod
#     def parent_job_template():
#         parent_job_template = """
# stages:
#   - build-libs
# .basic:
#   interruptible: false
#   allow_failure: false
#   retry:
#     max: 2
#     when: 
#       - runner_system_failure
# """
#         return parent_job_template

#     @staticmethod
#     def child_pipeline_job_template(lib):
#         child_pipeline_job_template = f"""
# build-{lib}-lib:
#   extends: .basic
#   stage: build-libs
#   variables:
#      BUILD_LIB: {lib}
#   script:
#     - echo $BUILD_LIB 
# """
#         return child_pipeline_job_template

    @staticmethod
    def header():
        header = f"""
stages:
  - deploy
"""
        return header

    @staticmethod
    def dynamic_pipeline(p):
        dynamic_pipeline = f"""
build-{p}-to-caas:
  stage: deploy
  variables:
    env: dev
    DEPLOY_ENVIRONMENT: dev
  trigger:
    project: sharmar0790/{p}
    strategy: depend

"""
        return dynamic_pipeline
